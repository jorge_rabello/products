package br.com.jorgerabellodev.products.domain.entities;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "product")
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "code", length = 5, nullable = false)
  private String code;

  @Column(name = "description", length = 200, nullable = false)
  private String description;

  @Column(name = "price", precision = 3, nullable = false)
  private BigDecimal price;

  @Column(name = "category", length = 100, nullable = false)
  private String category;
}
