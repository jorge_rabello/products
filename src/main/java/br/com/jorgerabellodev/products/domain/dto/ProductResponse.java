package br.com.jorgerabellodev.products.domain.dto;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@RequiredArgsConstructor
public class ProductResponse {

  private final Long id;
  private final String code;
  private final String description;
  private final BigDecimal price;
  private final String category;

}
