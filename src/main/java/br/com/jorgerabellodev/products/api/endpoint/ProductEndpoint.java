package br.com.jorgerabellodev.products.api.endpoint;

import br.com.jorgerabellodev.products.api.service.ProductService;
import br.com.jorgerabellodev.products.domain.dto.ProductRequest;
import br.com.jorgerabellodev.products.domain.dto.ProductResponse;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/product")
public class ProductEndpoint {

  private final ProductService service;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findAll() {
    List<ProductResponse> allProducts = service.findAll();
    return new ResponseEntity<>(allProducts, HttpStatus.OK);
  }

  @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findById(@PathVariable("id") Long id) {
    ProductResponse product = service.findById(id);
    return new ResponseEntity<>(product, HttpStatus.OK);
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> newProduct(@RequestBody ProductRequest productRequest) {
    ProductResponse savedProduct = service.save(productRequest);
    return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
  }

  @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> updateProduct(@RequestBody ProductRequest productRequest, @PathVariable("id") Long id) {
    ProductResponse updatedProduct = service.update(productRequest, id);
    return new ResponseEntity<>(updatedProduct, HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteProduct(@PathVariable("id") Long id) {
    service.delete(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
