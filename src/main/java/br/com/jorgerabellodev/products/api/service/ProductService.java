package br.com.jorgerabellodev.products.api.service;

import br.com.jorgerabellodev.products.api.errors.exceptions.ResourceNotFoundException;
import br.com.jorgerabellodev.products.api.repository.ProductRepository;
import br.com.jorgerabellodev.products.domain.dto.ProductRequest;
import br.com.jorgerabellodev.products.domain.dto.ProductResponse;
import br.com.jorgerabellodev.products.domain.entities.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {

  private final ProductRepository repository;

  public List<ProductResponse> findAll() {
    List<Product> allProducts = repository.findAll();
    List<ProductResponse> allProductResponse = new ArrayList<>();

    allProducts.forEach(product -> {
      ProductResponse productResponse = ProductResponse
          .builder()
          .id(product.getId())
          .code(product.getCode())
          .description(product.getDescription())
          .price(product.getPrice())
          .category(product.getCategory())
          .build();
      allProductResponse.add(productResponse);
    });
    return allProductResponse;
  }

  public ProductResponse findById(Long id) {
    Optional<Product> product = repository.findById(id);
    if (!product.isPresent()) {
      throw new ResourceNotFoundException("Product not found for id: " + id);
    }

    return ProductResponse
        .builder()
        .id(product.get().getId())
        .code(product.get().getCode())
        .description(product.get().getDescription())
        .price(product.get().getPrice())
        .category(product.get().getCategory())
        .build();
  }

  public ProductResponse save(ProductRequest productRequest) {

    Product product = Product
        .builder()
        .code(productRequest.getCode())
        .description(productRequest.getDescription())
        .price(productRequest.getPrice())
        .category(productRequest.getCategory())
        .build();

    Product savedProduct = repository.save(product);

    return ProductResponse
        .builder()
        .id(savedProduct.getId())
        .code(savedProduct.getCode())
        .description(savedProduct.getDescription())
        .price(savedProduct.getPrice())
        .category(savedProduct.getCategory())
        .build();
  }

  public ProductResponse update(ProductRequest productRequest, Long id) {
    Optional<Product> optionalProduct = repository.findById(id);
    if (!optionalProduct.isPresent()) {
      throw new ResourceNotFoundException("Product not found by id: " + id);
    }

    Product product = Product
        .builder()
        .id(id)
        .code(productRequest.getCode())
        .description(productRequest.getDescription())
        .price(productRequest.getPrice())
        .category(productRequest.getCategory())
        .build();

    Product updatedProduct = repository.save(product);

    return ProductResponse
        .builder()
        .id(updatedProduct.getId())
        .code(updatedProduct.getCode())
        .description(updatedProduct.getDescription())
        .price(updatedProduct.getPrice())
        .category(updatedProduct.getCategory())
        .build();
  }

  public void delete(Long id) {
    Optional<Product> optionalProduct = repository.findById(id);
    if (!optionalProduct.isPresent()) {
      throw new ResourceNotFoundException("Product not found by id: " + id);
    }
    repository.deleteById(id);
  }
}
