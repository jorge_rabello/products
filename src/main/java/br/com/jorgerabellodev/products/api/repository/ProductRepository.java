package br.com.jorgerabellodev.products.api.repository;

import br.com.jorgerabellodev.products.domain.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
